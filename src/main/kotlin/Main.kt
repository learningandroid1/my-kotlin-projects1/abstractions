import kotlin.random.Random

fun main() {
    val train = Train(10000)
    println("Capacity - ${train.capacity}")
    train.move()
    println(train.hashCode())

    val seat1 = Seat(5, 'D')
    println("Seat - ${seat1.row}${seat1.letter}")

    val passenger1 = Passenger("Vasya", "Kulakov", "651akwhe", seat1)
    println("Passenger - ${passenger1.name} ${passenger1.lastName}, passport ${passenger1.passport}, seat - $seat1")

    val boeing737 = Boeing737()
    val passengersCount = Random.nextInt(1, boeing737.capacity)
    for (i in 0 until passengersCount){
        val seat = boeing737.getAvailableSeat() ?: return

        val passenger = Passenger(
            name = "Ivan",
            lastName = "Petrov",
            passport = "${Random.nextInt(1000, 9000)} ${Random.nextInt(100000, 999999)}",
            seat = seat
        )
        boeing737.addPassenger(passenger)
    }
    println(boeing737.getInfo())
    boeing737.getSeatScheme()

    val zeppelin = Zeppelin()
    val zeppelinPassengers = Random.nextInt(1, zeppelin.capacity)
    for (i in 0 until zeppelinPassengers){
        val seat = zeppelin.getAvailableSeat() ?: return

        val passenger = Passenger(
            name = "Ivan",
            lastName = "Petrov",
            passport = "${Random.nextInt(1000, 9000)} ${Random.nextInt(100000, 999999)}",
            seat = seat
        )
        zeppelin.addPassenger(passenger)
    }
    println(zeppelin.getInfo())
    zeppelin.getSeatScheme()
}