abstract class AirCraft (maxWeightARg: Int): Transporter (maxWeightARg) {
    abstract val brand: String
    abstract val model: String
    abstract val engineCount: Int
    abstract val altitude: Int

    abstract val rows: Int
    abstract val numberOfSeatsInRow: Int

    protected val seatScheme by lazy {
        List(rows) {                                      // в листе row элементов, каждый из которых - лист Passenger, количеством numberOfSeatsInRow
            MutableList<Passenger?>(numberOfSeatsInRow) { // либо Passenger, либо null
                null
            }
        }
    }

    fun addPassenger(passenger: Passenger){
        val row = passenger.seat.row // у инстанса Passenger есть инстанс Seat, у которого есть row.
        val number = passenger.seat.letter - 'A'
        seatScheme[row][number] = passenger // passenger из параметров запихивается в seatScheme на место [row][number]
    }

    fun getPassenger(seat: Seat): Passenger?{
        val row = seat.row // у инстанса Passenger есть инстанс Seat, у которого есть row.
        val number = seat.letter - 'A'
        return seatScheme[row][number] // возвращает пассажира, коий есть элемент в массиве seatScheme, по адресу [row][number]
    }

    abstract fun getSeatScheme()

    fun getInfo() = """Aircraft: $brand $model 
        |Max Weight: $maxWeight
        |Capacity: $capacity
        |Number of rows: $rows
        |Number of seats in a row: $numberOfSeatsInRow
    """.trimMargin()

    override fun move() {
        println("Aircraft flies")
    }

    fun getAvailableSeat(): Seat? {
        val availableSeat = mutableListOf<Seat>()
        seatScheme.forEachIndexed { rowIndex, row->
            row.forEachIndexed { seatIndex, passenger ->
                if (passenger == null) {
                    availableSeat.add(Seat(rowIndex, 'A' + seatIndex))
                }
            }
        }
        return availableSeat.randomOrNull()
    }
}